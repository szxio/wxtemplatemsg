package com.szx.java.controller;


import com.szx.java.entity.WToken;
import com.szx.java.service.WTokenService;
import com.szx.java.utils.Response;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author szx
 * @since 2023-05-30
 */
@Api(tags = "token管理")
@RestController
@RequestMapping("/wtoken")
public class WTokenController {

    @Autowired
    WTokenService tokenService;

    @ApiOperation("微信接口配置，响应微信发送的Token验证")
    @GetMapping
    public String checkToken(HttpServletRequest request, HttpServletResponse response){
        return tokenService.checkToken(request,response);
    }

    @ApiOperation("相应微信消息")
    @PostMapping
    public String postWeChar(HttpServletRequest request, HttpServletResponse response){
        return tokenService.postWeChar(request,response);
    }

    @ApiOperation("获取AccessToken")
    @GetMapping("getAccessToken")
    public Response<String> gotAccessToken(){
        String accessToken = tokenService.getAccessToken();
        return Response.rspData(accessToken);
    }

    @ApiOperation("发送模板消息")
    @GetMapping("sendTemplateMsg")
    public void sendTemplateMsg(){
        tokenService.sendTemplateMsg();
    }

}

