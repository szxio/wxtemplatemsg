package com.szx.java.controller;
import com.szx.java.service.impl.FileServiceImpl;
import com.szx.java.utils.Response;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;

@Api(tags = "文件上传")
@RestController
public class FileUploadController {
    @Autowired
    FileServiceImpl fileService;

    @ApiOperation("分片上传")
    @PostMapping("/splitUpload")
    public Response<Boolean> uploadFileByCondition(MultipartFile file, int chunkNumber, int totalChunks){
       return Response.rspData(fileService.uploadFileByCondition(file,chunkNumber,totalChunks));
    }

    @GetMapping("/downloadFileByPath")
    public void downloadFileByPath(String path, HttpServletResponse response){
        fileService.downloadFileByPath(path,response);
    }

    @ApiOperation("分片上传到OSS")
    @PostMapping("/ossUpload")
    public Response<String> ossUpload(MultipartFile file){
        return Response.rspData(fileService.ossUpload(file));
    }

}

