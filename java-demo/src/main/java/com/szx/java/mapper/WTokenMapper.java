package com.szx.java.mapper;

import com.szx.java.entity.WToken;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author szx
 * @since 2023-05-30
 */
public interface WTokenMapper extends BaseMapper<WToken> {

}
