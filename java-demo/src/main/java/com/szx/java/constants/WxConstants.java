package com.szx.java.constants;


import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @author songzx
 * @create 2023-05-30 16:56
 */
@Component
public class WxConstants implements InitializingBean {
    @Value("${wx.appid}")
    private String wxAppid;

    @Value("${wx.secret}")
    private String wxSecret;

    @Value("${wx.template_id}")
    private String wxTemplateId;

    @Value("${wx.check_token}")
    private String wxCheckToken;

    public static String WX_APPID;
    public static String WX_SECRET;
    public static String WX_TEMPLATEID;
    public static String WX_CHECK_TOKEN;

    @Override
    public void afterPropertiesSet() throws Exception {
        WxConstants.WX_APPID = wxAppid;
        WxConstants.WX_SECRET = wxSecret;
        WxConstants.WX_TEMPLATEID = wxTemplateId;
        WxConstants.WX_CHECK_TOKEN = wxCheckToken;
    }
}
