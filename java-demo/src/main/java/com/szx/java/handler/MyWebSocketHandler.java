package com.szx.java.handler;

import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

/**
 * @author songzx
 * @create 2023-06-08 18:33
 */
public class MyWebSocketHandler extends TextWebSocketHandler{
    // 静态变量，用于存储WebSocketSession对象
    private static WebSocketSession session;

    @Override
    public void afterConnectionEstablished(WebSocketSession session) throws Exception {
        // 当WebSocket连接建立时，将WebSocketSession对象存储起来，以便后续使用
        // 这里可以使用自己的方式将session对象存储，例如将其放入Map中，或者存储到用户的会话中
        // 示例中使用静态变量存储WebSocketSession对象
        MyWebSocketHandler.session = session;
    }

    @Override
    protected void handleTextMessage(WebSocketSession session, TextMessage message) throws Exception {
        // 处理WebSocket文本消息
    }
    // 获取WebSocketSession对象的静态方法
    public static WebSocketSession getSession() {
        return session;
    }
}
