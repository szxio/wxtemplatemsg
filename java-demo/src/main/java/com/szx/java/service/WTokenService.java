package com.szx.java.service;

import com.szx.java.entity.WToken;
import com.baomidou.mybatisplus.extension.service.IService;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author szx
 * @since 2023-05-30
 */
public interface WTokenService extends IService<WToken> {
    String checkToken(HttpServletRequest request, HttpServletResponse response);

    String getAccessToken();

    String postWeChar(HttpServletRequest request, HttpServletResponse response);

    void sendTemplateMsg();
}
