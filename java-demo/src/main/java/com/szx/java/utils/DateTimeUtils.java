package com.szx.java.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;

/**
 * @author songzx
 * @date 2023/6/4
 * @apiNote
 */
public class DateTimeUtils {
    /**
     * 拿当前时间和传递过来时间做比较，如果当前时间小于传递进来的时间，则返回负数，否则返回正数
     */
    public static int CompareTime(String time){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        LocalDateTime dateTime = LocalDateTime.parse(time, formatter);
        Instant instant = dateTime.atZone(ZoneId.systemDefault()).toInstant();
        long timestamp = instant.toEpochMilli();

        long currentTimestamp = System.currentTimeMillis();
        return Long.compare(currentTimestamp, timestamp);
    }

    /**
     * 更新过期时间
     */
    public static String FutureTime(){
        int seconds = 7000;
        LocalDateTime dateTime = LocalDateTime.now().plusSeconds(seconds);
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        return dateTime.format(formatter);
    }
}
