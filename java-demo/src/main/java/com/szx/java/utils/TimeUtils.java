package com.szx.java.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * @author songzx
 * @create 2023-05-30 16:53
 */
public class TimeUtils {
    // 获取当前时间
    public static String now(Integer amount){
        Integer s = 0;
        if(amount != null){
            s = amount;
        }
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.SECOND, s);   //2小时之后的时间
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String token_yxq = sdf.format(calendar.getTime());
        return token_yxq;
    }

    // 时间转时间戳
    public static long timeToTimestamp(String s) {
        String res;
        //设置时间模版
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = null;
        try {
            date = simpleDateFormat.parse(s);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date.getTime();
    }
}
